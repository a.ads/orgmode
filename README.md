# Exemplo de projeto usando o Org Mode do Emacs

O propósito desse repositório é mostrar como usar o Gitlab para fazer [Integração
Contínua](https://gitlab.com/help/ci/quick_start/README.md) com um projeto que faz uso do [Org Mode](http://orgmode.org) do Emacs.

Para executar esse projeto, simplesmente faça um fork dele no gitlab.com. Então,
todo *push* irá disparar um novo *build* no gitlab.

## Fonte

Esse projeto é inspirado no [projeto exemplo para a linguagem
PHP](https://gitlab.com/gitlab-examples/php).
